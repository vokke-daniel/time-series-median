<?php
/**
 * Created by PhpStorm.
 * User: owner
 * Date: 25/08/2016
 * Time: 2:27 PM
 */

error_reporting(E_ALL);

require 'src/Median.php';

define("MAXSCANS", 16);

foreach (glob(__DIR__ . '/Time Series/originals_bak/*.*') as $fileName) {
    $fileList[] = $fileName;
}

$series_list = array();

foreach ($fileList as $f) {
    $handle = fopen($f, 'r') or die ("Failed to open file!");

    for ($i = 0; $i < MAXSCANS; $i++) {
        $series_list[$i][] = (double)(fgets($handle));
    }

    fclose($handle);
}

for ($i = 0; $i < MAXSCANS ; $i++) {
    array_multisort($series_list[$i]);
}

$count = 1;
foreach ($series_list as $series) {
    echo "SERIES " . $count . " (SIZE: " . count($series) .
        ")\n--------------------\n";
    foreach ($series as $value) {
        echo $value . "\n";
    }
    echo "\n";
    echo "MEDIAN " . $count . ": " . Median::get_median($series) . "\n\n";
    $count++;
}

$medians = array();
foreach ($series_list as $series) {
    $medians[] = Median::get_median($series);
}

$export_file = fopen(__DIR__ . '/Time Series/medians.csv', 'w');
fputcsv($export_file, $medians);
fclose($export_file);